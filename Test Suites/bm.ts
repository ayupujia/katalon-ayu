<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>bm</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>66e35195-f57c-4f16-a526-31b2d30df6ef</testSuiteGuid>
   <testCaseLink>
      <guid>a24b3348-45b9-4c3b-a928-10bc0897ef0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/pre test day 1/bankManager</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>51c62a30-6c26-462f-8951-ac639ff2254d</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/XYZ Custtomer XL</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>51c62a30-6c26-462f-8951-ac639ff2254d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>First Name</value>
         <variableId>d24cfe20-b307-40e8-bc0c-9e98d38574b3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>51c62a30-6c26-462f-8951-ac639ff2254d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Last Name</value>
         <variableId>9cc9fd87-c155-4529-8eac-119b7529fe8e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>51c62a30-6c26-462f-8951-ac639ff2254d</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Post Code</value>
         <variableId>61e153ec-09d0-4d77-bdab-0fec85b8d410</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
